import json
import gzip
import urllib.request
from connect import create_connection
import datetime


page_url = 'https://snap.datastream.center/techquest/'

def is_json(myjson):
  try:
    json_object = json.loads(myjson)
  except ValueError as e:
    return False
  return True

def push_to_db(report, date, format):
    api_report = page_url + report + '-' + date + '.' + format + '.gz'
    file = urllib.request.urlopen(api_report)
    f = gzip.open(file, 'rb')
    file_content = f.read()
    data = json.loads(json.dumps(file_content.decode("utf-8"))).split('\n')
    sql = ''
    data = [value for value in data if value]
    for i in data:
        user = i.split('user":')[-1].split(',"ts"')[0]
        ts = i.split('ts":')[-1].split(',"context"')[0]
        context = i.split('context":')[-1].split(',"ip"')[0]
        ip = i.split('ip":')[-1].split("}")[0]
        if user.isdigit() == False:
            sql = sql+"INSERT INTO data_error VALUES ('{api_report}','{api_date}','{row_text}','User is not int','{time}');"\
             .format(api_report=api_report, api_date=date, row_text=context, time=datetime.datetime.now())
        elif is_json(context) == False:
            sql = sql+"INSERT INTO data_error VALUES ('{api_report}','{api_date}','{row_text}','Context is not json','{time}');"\
             .format(api_report=api_report, api_date=date, row_text=context, time=datetime.datetime.now())
        else:
            sql = sql + "INSERT INTO report_input  VALUES ({user},'{ts}','{context}','{ip}');"\
             .format(user=user, ts=datetime.datetime.fromtimestamp(float(ts)).strftime("%Y-%m-%d %H:%M:%S"), context=context, ip=ip)
    cnx = create_connection("localhost", "root", "TytParolNaprimerMittenwald", 'test')
    cursor = cnx.cursor()
    for result in cursor.execute(sql, multi=True):
        pass
    cnx.commit()
    cursor.close()
    cnx.close()
    cnx.disconnect()

push_to_db('input','2017-02-01','json')
