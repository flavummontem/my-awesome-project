CREATE table report_input 
(user_id int,
ts timestamp,
context json,
ip varchar(100)
);

CREATE table data_error 
(api_report varchar(100),
api_date date,
row_text varchar(100),
error_text varchar(100),
ins_ts timestamp
);
